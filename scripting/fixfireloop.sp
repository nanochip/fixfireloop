#pragma semicolon 1
#include <sourcemod>
#include <sdktools>

#define PLUGIN_VERSION "1.0"

public Plugin myinfo = {
	name		= "Fix Flamethrower Fire Loop Bug",
	author		= "Nanochip",
	description = "yep",
	version		= PLUGIN_VERSION,
	url			= "http://steamcommunity.com/id/xNanochip/"
};

bool g_bIgnoreHook;

public void OnPluginStart()
{
	CreateConVar("sm_fixfireloop_version", PLUGIN_VERSION, "Fix Fire Loop Version", FCVAR_SPONLY|FCVAR_UNLOGGED|FCVAR_DONTRECORD|FCVAR_REPLICATED|FCVAR_NOTIFY);
	AddTempEntHook("TFExplosion", Sound_OnExplosion);
}

public Action Sound_OnExplosion( const char[] te_name, const int[] clients, int numClients, float delay )
{
	if (g_bIgnoreHook)
	{
		g_bIgnoreHook = false;
		return Plugin_Stop;
	}

	// OKAY. so valve introduced some bug where reflecting a rocket into the world would make the client emit a looping flamethrower sound
	// based on my observations, there's two possibilities:
	// 
	//   1. the server is incorrectly creating TFExplosion TEs that the client mis-interprets and then proceeds to play looped sounds with
	//   2. there's some networked data race where the client is processing a TE's networked fields before they're fully up to date (can this even happen? it's valve, who knows)
	// 
	// another observation is that when the m_nDefID field is either not correctly assigned by the server (and is set to -1), the client will not play the weapon replacement sound
	// that makes the flamethrower loop forever
	
	TE_WriteNum("m_nDefID", -1);

	g_bIgnoreHook = true;

	// send our modified TE
	TE_Send(clients, numClients);

	// stop the original
	return Plugin_Stop;
}